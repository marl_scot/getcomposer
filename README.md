Get Composer Install Script

This script will automaticly download
the latest composer installer, verify
its file hash and run it.

To run the installer from scratch on Debian based systems :

apt-get install wget ca-certificates

wget --no-check-certificate https://bitbucket.org/marl_scot/getcomposer/raw/master/setup

bash setup